#!/usr/bin/env bash

# init variable, if not set
CUSTOM_HUE_ROTATE=0

# shellcheck disable=SC1091
source ".env"

# add unit to the value
DEG="$CUSTOM_HUE_ROTATE""deg"

# assemble the CSS
CSS="body{filter: hue-rotate($DEG);}i.fa,svg{filter: hue-rotate(-$DEG);}"

# copy original CSS
docker cp \
    "$CONTAINER_NAME:/pgadmin4/pgadmin/static/js/generated/pgadmin.css" \
    pgadmin.css

# append CSS to original CSS, if original CSS is not already customized
[[ $(tail -n 1 pgadmin.css) == "$CSS" ]] ||
    echo -e "\n$CSS" >>pgadmin.css
