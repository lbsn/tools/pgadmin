# Docker compose for pgadmin database interface

This repository is a [Docker]-project, that starts an instance of [dpage]'s version of [pgAdmin]. It is supposed to be utilized together with [rawdb] and [hlldb] databases. You can also download/clone and run [all of these services as a bundle][full-stack-lbsn].

**tl;dr**

To Start the docker container locally:

```bash
git clone git@gitlab.vgiscience.de:lbsn/tools/pgadmin.git
cd pgadmin
cp .env.example .env # Optional edit
cp servers.json.example servers.json
docker network create lbsn-network
docker-compose up -d
```

## Configuration

Copy `.env.example` to `.env` and adjust to your needs. `PGADMIN_WEBURL` is only relevant for server deployments.

### Pre-configured servers

If you want to pre-configure servers, rename `servers.json.example` to `servers.json` and add your content. Then, add `:docker-compose.servers.yml` to the list of compose files (`COMPOSE_FILE` variable) in your `.env` file.

### Custom CSS

You can customize the appearance of your pgAdmin instance ([hue-rotate] the color) to have a better visual distinction from other instances:

![Custom CSS](custom-css.png)

First, add an environment-variable `CUSTOM_HUE_ROTATE=` with any positive angle value to your `.env` file. Then start-up the instance and execute `custom-css.sh`. It will copy the original stylesheet out of the container and append it with some CSS applying the `hue-rotate`-filter. Finally, add `:docker-compose.css.yml` to the list of compose files in your `.env` file and restart the container. Your pgAdmin will now appear in a different color according to the value you chose (might need some cache refresh: `Shift+F5`).

[Docker]: https:/www.docker.com
[pgAdmin]: https://www.pgadmin.org
[dpage]: https://hub.docker.com/r/dpage/pgadmin4/
[rawdb]: https://gitlab.vgiscience.de/lbsn/databases/rawdb
[hlldb]: https://gitlab.vgiscience.de/lbsn/databases/hlldb
[full-stack-lbsn]: https://gitlab.vgiscience.de/lbsn/tools/full-stack-lbsn
[hue-rotate]: https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/hue-rotate
